#!/bin/bash

DTBO_FILES_SRC=/fw/dtbo/dtbo
SYSTEM_FW_PATH=/lib/firmware

CONFIG_FS_PATH=/sys/kernel/config


DTBO_FILES=$(ls -1 $DTBO_FILES_SRC/*.dtbo)
# Add each DTBO file in current directory to full/path
for DTBO_FILE in $DTBO_FILES;
do 
    #get raw name
    name=$(basename -s.dtbo $DTBO_FILE)
    #generate symlink target
    target=$SYSTEM_FW_PATH/$name.dtbo

    #remove this overlay if it exists
    if [ -d $CONFIG_FS_PATH/device-tree/overlays/$name ]; then
	#create the directory in the device-tree configfs directory
	rmdir $CONFIG_FS_PATH/device-tree/overlays/$name
    fi

    #create the symlink if it doesn't already exist
    if [ -f $target ]; then
	rm $target
    fi
    
done
