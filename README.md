# DeviceTreeOverlay-scripts

Scripts for loading and unloading device tree overlays on a linux system.

These scripts currently assume an already mounted configfs parition in /sys/kernel/config and a fw directory of /lib/firmware. 
These are very preliminary scripts, so don't use them without understanding them. 

Based on instructions from [here](https://xilinx-wiki.atlassian.net/wiki/spaces/A/pages/18841645/Solution+Zynq+PL+Programming+With+FPGA+Manager#SolutionZynqPLProgrammingWithFPGAManager-UsingDeviceTreeOverlay%3A)