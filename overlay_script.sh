#!/bin/bash

DTBO_FILES_SRC=/fw/dtbo/dtbo
SYSTEM_FW_PATH=/lib/firmware

CONFIG_FS_PATH=/sys/kernel/config


DTBO_FILES=$(ls -1 $DTBO_FILES_SRC/*.dtbo)
# Add each DTBO file in current directory to full/path
for DTBO_FILE in $DTBO_FILES;
do 
    #get raw name
    name=$(basename -s.dtbo $DTBO_FILE)
    #generate symlink target
    target=$SYSTEM_FW_PATH/$name.dtbo
    #create the symlink if it doesn't already exist
    if [ ! -f $target ]; then
	ln -s $DTBO_FILE $target
    fi
    #if the FW isn't already loaded, load it
    if [ ! -d $CONFIG_FS_PATH/device-tree/overlays/$name ]; then
	#create the directory in the device-tree configfs directory
	mkdir -p $CONFIG_FS_PATH/device-tree/overlays/$name
	#echo the dtbo filename into the path variable in the new directory
	$(echo -n "$name.dtbo" > $CONFIG_FS_PATH/device-tree/overlays/$name/path)
    fi
    
done
